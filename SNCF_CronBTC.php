<?php

include './KeysBTC.php';
include './functions.php';

$api_sncf_user = $api_sncf_user_key;
$api_sncf_mdp  = '';

if($SensWay == "BTC"){
  $sens = "Brioude -> Clermont-ferrand";
  $VilleDepart  = 'admin:fr:63052'; // Le Breuil sur Couze ( Mairie stop area -> stop_area:OCE:SA:87734228 )
  $VilleArrivee = 'admin:fr:63113'; // Clermont-ferrand (la pardieu stop area -> stop_area:OCE:SA:87782607 )
  $stopAreaDepart = 'stop_area:OCE:SA:87734228'; // breuil mairie
  $stopAreaArrivée = 'stop_area:OCE:SA:87782607'; // La pardieu
} elseif($SensWay == "CTB") {
  $sens = "Clermont-ferrand -> Brioude";
  $VilleDepart  = 'admin:fr:63113'; // Clermont-ferrand ( La pardieu stop area -> stop_area:OCE:SA:87782607 )
  $VilleArrivee = 'admin:fr:63052'; // Le Breuil sur Couze (Mairie stop area -> stop_area:OCE:SA:87734228 )
  $stopAreaDepart = 'stop_area:OCE:SA:87782607'; // La pardieu
  $stopAreaArrivée = 'stop_area:OCE:SA:87734228'; // breuil mairie
} else {
  $sens = "non reconnu en V1 -- Default function activated";
  $VilleDepart  = 'admin:fr:63052'; // Le Breuil sur Couze ( Mairie stop area -> stop_area:OCE:SA:87734228 )
  $VilleArrivee = 'admin:fr:63113'; // Clermont-ferrand (la pardieu stop area -> stop_area:OCE:SA:87782607 )
  $stopAreaDepart = 'stop_area:OCE:SA:87734228'; // breuil mairie
  $stopAreaArrivée = 'stop_area:OCE:SA:87782607'; // La pardieu
}

getDepartureFromStopArea($api_sncf_user, $stopAreaDepart, $date);
//getArivalAtStopArea($api_sncf_user, $stopAreaArrivée, $date);
//getStopScheduledAtStopArea($api_sncf_user, $stopAreaDepart, $date, $NbItems);
//getRouteScheduledAtStopArea($api_sncf_user, $stopAreaDepart, $date, $NbItems);
//getTrains($api_sncf_user, $api_sncf_mdp, $VilleDepart, $VilleArrivee, $date,$NbJourneys,$sens);

?>
