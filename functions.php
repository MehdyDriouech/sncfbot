<?php

$date = date("Ymd\TH:i");
$NbJourneys ="8"; //Nb de trains minimum par jour à afficher
$NbItems ="4";

function sendNotificationMail($sender, $receiver, $date, $msg){

  $from = $sender;
  $to = $receiver;
  $subject = 'Horaires du jour : '.$date;

  $headers = "MIME-Version: 1.0\r\n";
  $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
  $headers .= "From: Moonshine_SNCF_BOT <$from>\r\n";

  $msg = $msg;

  mail($to, $subject, $msg, $headers) or die ("mail error");
  echo 'Notification Mail envoyée <br/>';

}

function sendNotificationMSTeams($msg){

    $url = "";
    $ch = curl_init();

    $jsonData = array(
        'text' => ' '.$msg.' '
    );

    $jsonDataEncoded = json_encode($jsonData, true);

    $header = array();
    $header[] = 'Content-type: application/json';

    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

    $result = curl_exec($ch);
    curl_close($ch);

   echo 'Notification MSTeams envoyée';
}

function sendNotificationSMS($freemobileWebhook, $sensTrain,$msg){
// TODO //
}


function dateToFrenchFormat($time){

  $Days = substr($time, -9, 2);
  $Months = substr($time, -11, 2);
  $Years = substr($time, -15, 4);
  $Hours = substr($time, 9,-4);
  $Minutes = substr($time, 11,-2);
  $Seconds = substr($time, 13);

  $timeFR = " ".$Days."/".$Months."/".$Years."-".$Hours.":".$Minutes.":".$Seconds." ";
  $timeFR = strval($timeFR);
  return $timeFR;
}

function getDepartureFromStopArea($api_sncf_user, $stopArea, $date){

    try {

        $url= "https://".$api_sncf_user."@api.sncf.com/v1/coverage/sncf/stop_areas/".$stopArea."/departures?from_datetime=".$date;
        $data = file_get_contents($url);
        $resultdata =json_decode($data, true);

        $MyResultArray = $resultdata['departures'];
        foreach ($MyResultArray as $key => $value) {

            $stopPointLabel = $value['stop_point']['label'];
            $direction = $value['display_informations']['direction'];
            $lineName = $value['display_informations']['name'];
            $initialArrival = dateToFrenchFormat($value['stop_date_time']['base_arrival_date_time']);
            $initialDeparture = dateToFrenchFormat($value['stop_date_time']['base_departure_date_time']);
            $Arrival = dateToFrenchFormat($value['stop_date_time']['arrival_date_time']);
            $Departure = dateToFrenchFormat($value['stop_date_time']['departure_date_time']);
            $AdditionalInfos = $value['stop_date_time']['additional_informations'][0];
            $Status = $value['stop_date_time']['data_freshness'];

            if ($AdditionalInfos == '') {
              $AdditionalInfos = 'Aucune infos additionnelles';
            }

            if ($Status == 'base_schedule') {
              $Status = "à l'heure";
            } else {
              $Status = $Status;
            }

            $msg = "Trains en partance de : ".$stopPointLabel." et en direction de : ".$direction.".<br/> Nom de la ligne : ".$lineName.".<br/> Heure d'arrivée en gare initiale : ".$initialArrival." Arrive finalement à ".$Arrival.".<br/>Heure de départ initial : ".$initialDeparture." part finalement à : ".$Departure.".<br/>Statut : ".$Status.".<br/>Infos additionnelles : ".$AdditionalInfos.".</br></br>";
            //echo $msg;
            sendNotificationMSTeams($msg);
        }

//toudou Mapping

    } catch (Exception $e) {
        echo 'Exception reçue : ',  $e->getMessage(), "\n";
    }
}

function getArivalAtStopArea($api_sncf_user, $stopArea, $date){

    try {

        $url= "https://".$api_sncf_user."@api.sncf.com/v1/coverage/sncf/stop_areas/".$stopArea."/arrivals?from_datetime=".$date;
        $data = file_get_contents($url);
        $resultdata =json_decode($data, true);

        $var = "getArivalAtStopArea ↑";
        echo $var;

        //toudou Mapping

    } catch (Exception $e) {
        echo 'Exception reçue : ',  $e->getMessage(), "\n";
    }
}

function getStopScheduledAtStopArea($api_sncf_user, $stopArea, $date, $NbItems){

    try {

        $url= "https://".$api_sncf_user."@api.sncf.com/v1/coverage/sncf/stop_areas/".$stopArea."/stop_schedules?from_datetime=".$date."&items_per_schedule=".$NbItems;
        $data = file_get_contents($url);
        $resultdata =json_decode($data, true);

        $var = "getStopScheduledAtStopArea ↑";
        echo $var;

        //toudou Mapping

    } catch (Exception $e) {
        echo 'Exception reçue : ',  $e->getMessage(), "\n";
    }
}

function getRouteScheduledAtStopArea($api_sncf_user, $stopArea, $date, $NbItems){

    try {

        $url= "https://".$api_sncf_user."@api.sncf.com/v1/coverage/sncf/stop_areas/".$stopArea."/route_schedules?from_datetime=".$date."&items_per_schedule=".$NbItems;
        $data = file_get_contents($url);
        $resultdata =json_decode($data, true);

        $var = "getRouteScheduledAtStopArea ↑";
        echo $var;

        //toudou Mapping

    } catch (Exception $e) {
        echo 'Exception reçue : ',  $e->getMessage(), "\n";
    }
}

function getCityCodeAndStopAreaCode($api_sncf_user, $city){

    try {

        $url= "https://".$api_sncf_user."@api.sncf.com/v1/coverage/sncf/places?q=".$city;
        $data = file_get_contents($url);
        $resultdata =json_decode($data, true);

        $cityCode = "";
        $stopAreaCode = "";
        echo $cityCode;
        echo $stopAreaCode;

//Toudou - prepare for V2

    } catch (Exception $e) {
        echo 'Exception reçue : ',  $e->getMessage(), "\n";
    }
}

function getTrains($api_sncf_user, $api_sncf_mdp, $VilleDepart, $VilleArrivee, $date, $NbJourneys, $sens){

    try {
        $url= "https://".$api_sncf_user."@api.sncf.com/v1/coverage/sncf/journeys?from=".$VilleDepart."&to=".$VilleArrivee."&datetime=".$date."&datetime_represents=departure&min_nb_journeys=".$NbJourneys."";
        $data = file_get_contents($url);
        $resultdata =json_decode($data, true);

        $train_status = strval($resultdata['journeys'][0]['status']);
        $arrival_date_time = strval($resultdata['journeys'][0]['arrival_date_time']);
        $departure_date_time = strval($resultdata['journeys'][0]['departure_date_time']);
        $requested_date_time = strval($resultdata['journeys'][0]['requested_date_time']);
        $data_journey = $resultdata['journeys'][0];

        if ($train_status != ''){
          $train_status = $train_status;
        } else {
          $train_status = 'OK';
        }

        $msg = "Voici les charactéristiques du prochain train pour faire ".$sens." - Status : ".$train_status." - Heure de départ initial : ".dateToFrenchFormat($requested_date_time)." - Heure de départ prévue : ".dateToFrenchFormat($departure_date_time)." - Heure d'arrivée prévue : ".dateToFrenchFormat($arrival_date_time);
        echo $msg;
        sendNotificationMSTeams($msg);

        //Notification part goes here

    } catch (Exception $e) {
        echo 'Exception reçue : ',  $e->getMessage(), "\n";
    }
}


 ?>
